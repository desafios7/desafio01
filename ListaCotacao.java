import java.util.ArrayList;
import java.util.Scanner;

public class ListaCotacao {
    ArrayList lista = new ArrayList();

    public void addCot(String cotacao) {
        lista.add(cotacao);
    }

    public void removerCot(String cotacao) {
        lista.remove(cotacao);
    }

    public void changeCot(String cotacao, String newCotacao) {
        int i = lista.indexOf(cotacao);
        lista.set(i, newCotacao);
    }

    public void showCot() {
        System.out.println(lista);
    }

    public static void main(String[] args) {
        ListaCotacao lista = new ListaCotacao();

        lista.addCot("Nome");
        lista.addCot("CPF");
        lista.addCot("Idade");
        lista.showCot();
        lista.removerCot("CPF");
        lista.showCot();

    }


}
